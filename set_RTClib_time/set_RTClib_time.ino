/* Sketch to set the time of a RTC DS3231
   using a computer as the true time.
   Time is set as UTC through the companion
   python script.
*/


#include <Wire.h>
#include <SPI.h>
#include <RTClib.h>
#include <RTC_DS3231.h>

RTC_DS3231 RTC;
char date_in[] = "Oct 21 2015"; // Set date constant for when clock has no time
char time_in[] = "07:28:00"; // Set time constant for when the clock has no time
char msg_in[] = "Oct 21 2015 07:28:00";
String input= String(msg_in);
byte status;
void setup(){
	Serial.begin(115200); // Set a fast baudrate to minimise time difference between RTC and PC. It needs to be THE SAME that the Arduino board is using for serial communications
	Wire.begin();
	RTC.begin();
	if (! RTC.isrunning()) {
		Serial.println(F("RTC is NOT running! Setting time to THAT time"));
		// following line sets the RTC to THAT date & time
		RTC.adjust(DateTime(date_in, time_in));

	}
	Serial.println(F("Waiting for time string from the PC"));
	// following lines parse input from the PC through serial port
	while (Serial.available()==0){
		delay(1);
	}
	// Get the full line of Date Time from the Serial port
	status = Serial.readBytes(msg_in,20);
	msg_in[20]='\0';
	input = String(msg_in);
	input.substring(0,11).toCharArray(date_in,12);
	input.substring(12).toCharArray(time_in,9);
	RTC.adjust(DateTime(date_in,time_in));
}
void loop(){
	DateTime now = RTC.now();
	Serial.print(now.year(), DEC);
	Serial.print('-');
	Serial.print(now.month(), DEC);
	Serial.print('-');
	Serial.print(now.day(), DEC);
	Serial.print(' ');
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.println();
	delay(1000);
}