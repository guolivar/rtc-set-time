#The libraries required
import serial  
import time  
import datetime
 
#Details of the Arduino device
# typical ports are:
# Windows: 'COMX' where X is an integer ... check your device manager to figure that out
# Linux: '/dev/ttyUSBX' or '/dev/ttyACMX' depending on the hardware used to connect to the arduino. X is an integer ... I use 'ls -l /dev/tty*' to check for devices.
device='/dev/ttyUSB0'
# If you change the baud rate here you'll need to also change it on the companioni Python script
baud=115200
 
print "Trying...",device  
arduino = serial.Serial(device, baud)
#Print the response from the Arduino - to show the communiction has started.
print arduino.readline()    
#Get the current time
curr_time = datetime.datetime.utcnow()
print curr_time.strftime("%b %d %Y %H:%M:%S")
send = curr_time.strftime("%b %d %Y %H:%M:%S")
#Send the current time to the Arduino
arduino.write(send)
print "Time sent to Arduino"
print "This is the Arduino time"
#Receive the time string from above back from the Ardunio.
print arduino.readline()
print arduino.readline()
print arduino.readline()
print arduino.readline()
print arduino.readline()
