# RTC Time Setup

## Goal
This project aims to simplify the task of setting the time on a real time clock connected to an Arduino board.
At this moment the supported chips are:

* DS3231
 * Chronodot: [https://www.adafruit.com/product/255]
 * Adafruit's breakout board: [https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/overview]

 It **should** work on other similar chips like the DS3234 by changing the library call on the Arduino sketch but I haven't tested it.

## Requirements
You'll need a RTC chip and an Arduino/Genuino board and the Arduino IDE to load the sketch on the microcontroller. You can load firmware without the Arduino IDE but if you know how to do that, you probably don't need instructions on how to get this project to work.

Next you'll need a PC (developed and tested on Linux but _should_ work on any platform) with Python 2.7+ (haven't "Python3-proofed" it but it's simple enough that it shouldn't be a major headache).
Modules required:

* Serial [https://pypi.python.org/pypi/pyserial] [https://pypi.python.org/pypi/pyserial/2.7]
* Datetime 
* Time

## Use
This project has two components that work together:

1. Arduino sketch. This is the firmware that will run on the Arduino board to connect and set the RTC's time.
2. Python script. This is the script running on the host PC that sends the computer time to the Arduino board to update the RTC chip.

To set things up you first need to compile and upload the Arduino sketch to your arduino board. Then you'll need to connect the RTC chip to the arduino board and make sure that the RTC chip has a backup battery as it will otherwise lose its time when disconnected from the Arduino board. See the following guides for the connections:

* [http://docs.macetech.com/doku.php/chronodot_v2.0]
* [here|https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/wiring-and-test]

After the Arduino board has the firmware loaded and the RTC has been connected, then you can plug the Arduino board to the PC through its serial connection (the same way you uploaded the sketch). Now, go to the Python script and modify the appropriate line to indicate where the Arduino is connected ( **line 8** ). Now, run the Python script either by loading the script on your Python IDE or by running through the command line:

```
python <PATH-TO-PYTHON-SCRIPT>/set_time_fromPC.py
```

You should see a few messages from the microcontroller and then 5 lines with the time that the RTC has been set to which should correspond to your PC time.

## License
MIT
